package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.endpoint.IProjectEndpoint;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    private IProjectService projectService;

    private ISessionService sessionService;

    public ProjectEndpoint() {
        super(null);
    }

    public ProjectEndpoint(final IServiceLocator IServiceLocator) {
        super(IServiceLocator);
        this.projectService = IServiceLocator.getProjectService();
        this.sessionService = IServiceLocator.getSessionService();
    }

    @Override
    @WebMethod
    public void add(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "project", partName = "project") Project project
    ) {
        sessionService.validate(session);
        projectService.add(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public void createProjectWithName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createProjectWithNameAndDescription(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> findUserProjects(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        return projectService.findUserProjects(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProjects(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        projectService.removeAllProjects(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Project findProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(session);
        return projectService.findProjectById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) {
        sessionService.validate(session);
        return projectService.findProjectByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(session);
        return projectService.findProjectByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Project updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        sessionService.validate(session);
        return projectService.updateProjectById(session.getUserId(), id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Project updateProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        sessionService.validate(session);
        return projectService.updateProjectByIndex(session.getUserId(), index, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Project removeProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(session);
        return projectService.removeProjectById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Project removeProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) {
        sessionService.validate(session);
        return projectService.removeProjectByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public Project removeProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(session);
        return projectService.removeProjectByName(session.getUserId(), name);
    }

}