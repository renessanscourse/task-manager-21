package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.endpoint.IAuthEndpoint;
import ru.ovechkin.tm.api.service.IAuthService;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    private IAuthService authService;

    private ISessionService sessionService;

    public AuthEndpoint() {
        super(null);
    }

    public AuthEndpoint(final IServiceLocator IServiceLocator) {
        super(IServiceLocator);
        this.authService = IServiceLocator.getAuthService();
        this.sessionService = IServiceLocator.getSessionService();
    }

    @Nullable
    @Override
    @WebMethod
    public String getUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        return session.getUserId();
    }

    @Override
    @WebMethod
    public void login(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        authService.login(login, password);
        sessionService.open(login, password);
    }

    @Override
    @WebMethod
    public void logout(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        sessionService.validate(session);
        authService.logout();
    }

    @Override
    @WebMethod
    public void registry(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email")final String email
    ) {
        authService.registry(login, password, email);
    }

    @NotNull
    @Override
    @WebMethod
    public User showCurrentUserByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "userId", partName = "userId") final String userId
    ) {
        sessionService.validate(session);
        return authService.findUserByUserId(userId);
    }

    @Override
    @WebMethod
    public void updateProfileInfo(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "newLogin", partName = "newLogin") final String newLogin,
            @Nullable @WebParam(name = "newFirstName", partName = "newFirstName") final String newFirstName,
            @Nullable @WebParam(name = "newMiddleName", partName = "newMiddleName") final String newMiddleName,
            @Nullable @WebParam(name = "newLastName", partName = "newLastName") final String newLastName,
            @Nullable @WebParam(name = "newEmail", partName = "newEmail") final String newEmail
    ) {
        sessionService.validate(session);
        authService.updateProfileInfo(newLogin, newFirstName, newMiddleName, newLastName, newEmail);
    }

    @Override
    @WebMethod
    public void updatePassword(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "currentPassword", partName = "currentPassword") final String currentPassword,
            @Nullable @WebParam(name = "newPassword", partName = "newEmail") final String newPassword
    ) {
        sessionService.validate(session);
        authService.updatePassword(currentPassword, newPassword);
    }

}