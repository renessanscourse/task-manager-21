package ru.ovechkin.tm.endpoint;

import ru.ovechkin.tm.api.endpoint.IStorageEndpoint;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.api.service.IStorageService;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.enumirated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.io.*;

@WebService
public class StorageEndpoint extends AbstractEndpoint implements IStorageEndpoint {

    private IStorageService storageService;

    private ISessionService sessionService;

    public StorageEndpoint() {
        super(null);
    }

    public StorageEndpoint(final IServiceLocator IServiceLocator) {
        super(IServiceLocator);
        this.storageService = IServiceLocator.getStorageService();
        this.sessionService = IServiceLocator.getSessionService();
    }

    @Override
    @WebMethod
    public void dataBase64Save(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        storageService.dataBase64Save();
    }

    @Override
    @WebMethod
    public void dataBase64Load(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        storageService.dataBase64Load();
    }

    @Override
    @WebMethod
    public void dataBinarySave(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        storageService.dataBinarySave();
    }

    @Override
    @WebMethod
    public void dataBinaryLoad(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, ClassNotFoundException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        storageService.dataBinaryLoad();
    }

    @Override
    @WebMethod
    public void dataJsonJaxbSave(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, JAXBException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        storageService.dataJsonJaxbSave();
    }

    @Override
    @WebMethod
    public void dataJsonJaxbLoad(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws JAXBException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        storageService.dataJsonJaxbLoad();
    }

    @Override
    @WebMethod
    public void dataJsonMapperLoad(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        storageService.dataJsonMapperLoad();
    }

    @Override
    @WebMethod
    public void dataJsonMapperSave(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        storageService.dataJsonMapperSave();
    }

    @Override
    @WebMethod
    public void dataXmlJaxbSave(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, JAXBException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        storageService.dataXmlJaxbSave();
    }

    @Override
    @WebMethod
    public void dataXmlJaxbLoad(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, JAXBException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        storageService.dataXmlJaxbLoad();
    }

    @Override
    @WebMethod
    public void dataXmlMapperSave(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        storageService.dataXmlMapperSave();
    }

    @Override
    @WebMethod
    public void dataXmlMapperLoad(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        storageService.dataXmlMapperLoad();
    }

}