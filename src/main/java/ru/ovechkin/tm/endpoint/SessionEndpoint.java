package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.api.endpoint.ISessionEndpoint;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.dto.Fail;
import ru.ovechkin.tm.dto.Result;
import ru.ovechkin.tm.dto.Success;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @WebMethod
    public Session openSession(
            @WebParam(name =  "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) throws AccessForbiddenException {
        return serviceLocator.getSessionService().open(login, password);
    }

    @NotNull
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session", partName = "session") final Session session
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().close(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<Session> sessionsOfUser(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().getListSession(session);
    }

}