package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.ISessionRepository;
import ru.ovechkin.tm.api.service.IPropertyService;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;
import ru.ovechkin.tm.exeption.other.UserDoesNotExistException;
import ru.ovechkin.tm.exeption.user.AccessDeniedException;
import ru.ovechkin.tm.util.HashUtil;
import ru.ovechkin.tm.util.SignatureUtil;

import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    protected IServiceLocator IServiceLocator;

    private final ISessionRepository sessionRepository;

    public SessionService(@NotNull final IServiceLocator IServiceLocator, final ISessionRepository sessionRepository) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.IServiceLocator = IServiceLocator;
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = IServiceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final Session session) throws AccessForbiddenException{
        if (session == null) throw new AccessForbiddenException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessForbiddenException();
        if (session.getTimestamp() == null) throw new AccessForbiddenException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessForbiddenException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessForbiddenException();
        if (!sessionRepository.contains(session.getId())) throw new AccessForbiddenException();
    }

    @Override
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessForbiddenException();
        validate(session);
        @NotNull final String userId = session.getUserId();
        @Nullable final User user = IServiceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessForbiddenException();
        if (user.getRole() == null) throw new AccessForbiddenException();
        if (!role.equals(user.getRole())) throw new AccessForbiddenException();
    }

    @NotNull
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @Nullable final User user = IServiceLocator.getUserService().findByLogin(login);
        if (user == null) throw new UserDoesNotExistException() ;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.merge(session);
        return sign(session);
    }

    @NotNull
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) throw new AccessForbiddenException();
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = IServiceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @NotNull final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    public void signOutByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        final User user = IServiceLocator.getUserService().findByLogin(login);
        if (user == null) return;
        @NotNull final String userId = user.getId();
        sessionRepository.removeByUserId(userId);
    }

    public void signOutByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        sessionRepository.removeByUserId(userId);
    }

    @NotNull
    @Override
    public User getUser(@Nullable Session session) throws AccessForbiddenException {
        @NotNull final String userId = getUserId(session);
        return IServiceLocator.getUserService().findById(userId);
    }

    @NotNull
    @Override
    public String getUserId(@Nullable Session session) throws AccessForbiddenException {
        validate(session);
        return session.getUserId();
    }

    @NotNull
    @Override
    public List<Session> getListSession(@Nullable final Session session) throws AccessForbiddenException {
        validate(session);
        return sessionRepository.findByUserId(session.getUserId());
    }

    @Override
    public void close(@Nullable final Session session) throws AccessForbiddenException {
        validate(session);
        remove(session);
    }

    @Nullable
    public Session remove(@Nullable final Session session) {
        validate(session);
        sessionRepository.remove(session);
        return session;
    }

    @Override
    public void closeAll(@Nullable Session session) throws AccessForbiddenException {
        validate(session);
        sessionRepository.removeByUserId(session.getUserId());
    }

}