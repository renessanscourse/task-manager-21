package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private final String NAME = "/application.properties";

    @NotNull
    private final Properties properties = new Properties();

    public void init() throws Exception {
        final InputStream inputStream = PropertyService.class.getResourceAsStream(NAME);
        properties.load(inputStream);
    }

    @NotNull
    @Override
    public String getServiceHost() {
        final String propertyHost = properties.getProperty("server.host");
        final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @NotNull
    @Override
    public Integer getServicePort() {
        final String propertyPort = properties.getProperty("server.port");
        final String envPort = System.getProperty("server.port");
        String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        return properties.getProperty("session.salt");
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

}