package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

public interface IUserService extends IService<User> {

    @Nullable
    User create(String login, String password);

    @Nullable
    User create(String login, String password, String email);

    @Nullable
    User create(String login, String password, Role role);

    @NotNull
    User findById(String id);

    @Nullable
    User findByLogin(String login);

    User removeUser(User user);

    @Nullable
    User removeById(String adminId, String id);

    @Nullable
    User removeByLogin(String userId, String login);

    @Nullable
    User lockUserByLogin(String userId, String login);

    @Nullable
    User unLockUserByLogin(String userId, String login);

}