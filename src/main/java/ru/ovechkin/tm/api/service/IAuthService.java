package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

public interface IAuthService {

    @Nullable
    String getUserId();

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    void checkRoles(
            @NotNull Session session,
            @Nullable Role[] roles
    );

    boolean isAuth();

    @NotNull
    User findUserByUserId(@Nullable String id);

    void updateProfileInfo(
            @Nullable String newLogin,
            @Nullable String newFirstName,
            @Nullable String newMiddleName,
            @Nullable String  newLastName,
            @Nullable String newEmail
    );

    void updatePassword(@Nullable String currentPassword, @Nullable String newPassword);

}