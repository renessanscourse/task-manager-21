package ru.ovechkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull Session findById(@NotNull String id);

    @NotNull List<Session> findByUserId(@NotNull String userId);

    boolean contains(@NotNull String id);

    void remove(@NotNull Session session);

    void removeByUserId(@NotNull String userId);

}