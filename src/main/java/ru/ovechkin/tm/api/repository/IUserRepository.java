package ru.ovechkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User add(@NotNull User user);

    @NotNull
    User findById(@NotNull String id);

    @NotNull
    User findByLogin(@NotNull String login);

    @NotNull
    User removeUser(@NotNull User user);

    @NotNull
    User removeById(@NotNull String id);

    @NotNull
    User removeByLogin(@NotNull String login);

}