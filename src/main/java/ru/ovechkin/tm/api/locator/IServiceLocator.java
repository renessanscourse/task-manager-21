package ru.ovechkin.tm.api.locator;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.api.service.*;

public interface IServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IStorageService getStorageService();

}