package ru.ovechkin.tm.api.locator;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.api.endpoint.*;

public interface IEndpointLocator {

    @NotNull
    ISessionEndpoint getSessionEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    IStorageEndpoint getStorageEndpoint();

}