package ru.ovechkin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {


    @Nullable
    @WebMethod
    User findById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    User findByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

    @Nullable
    @WebMethod
    User removeUser(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "user", partName = "user") final User user
    );

    @Nullable
    @WebMethod
    User removeById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    User removeByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

    @Nullable
    @WebMethod
    User createWithLoginAndPassword(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    );

    @Nullable
    @WebMethod
    User createWithLoginPasswordAndEmail(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email")final String email
    );

    @Nullable
    @WebMethod
    User createWithLoginPasswordAndRole(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "role", partName = "role")final Role role
    );

    @Nullable
    @WebMethod
    User lockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "userId", partName = "userId")final String userid,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

    @Nullable
    @WebMethod
    User unLockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

}