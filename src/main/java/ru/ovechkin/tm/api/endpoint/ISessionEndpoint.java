package ru.ovechkin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.dto.Result;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ISessionEndpoint {

    @NotNull
    @WebMethod
    Session openSession(
            @WebParam(name =  "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password
    ) throws AccessForbiddenException;

    @NotNull
    @WebMethod
    Result closeSession(
            @WebParam(name = "session", partName = "session") Session session
    ) throws AccessForbiddenException;

    @NotNull
    @WebMethod
    List<Session> sessionsOfUser(
            @WebParam(name = "session", partName = "session") Session session
    );
}