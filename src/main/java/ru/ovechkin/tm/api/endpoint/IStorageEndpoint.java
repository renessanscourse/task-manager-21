package ru.ovechkin.tm.api.endpoint;

import ru.ovechkin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface IStorageEndpoint {
    @WebMethod
    void dataBase64Save(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    void dataBase64Load(
            @WebParam(name = "session", partName = "session") Session session
    ) throws Exception;

    @WebMethod
    void dataBinarySave(
            @WebParam(name = "session", partName = "session") Session session
    ) throws IOException;

    @WebMethod
    void dataBinaryLoad(
            @WebParam(name = "session", partName = "session") Session session
    ) throws IOException, ClassNotFoundException;

    @WebMethod
    void dataJsonJaxbSave(
            @WebParam(name = "session", partName = "session") Session session
    ) throws IOException, JAXBException;

    @WebMethod
    void dataJsonJaxbLoad(
            @WebParam(name = "session", partName = "session") Session session
    ) throws JAXBException;

    @WebMethod
    void dataJsonMapperLoad(
            @WebParam(name = "session", partName = "session") Session session
    ) throws IOException;

    @WebMethod
    void dataJsonMapperSave(
            @WebParam(name = "session", partName = "session") Session session
    ) throws IOException;

    @WebMethod
    void dataXmlJaxbSave(
            @WebParam(name = "session", partName = "session") Session session
    ) throws IOException, JAXBException;

    @WebMethod
    void dataXmlJaxbLoad(
            @WebParam(name = "session", partName = "session") Session session
    ) throws IOException, JAXBException;

    @WebMethod
    void dataXmlMapperSave(
            @WebParam(name = "session", partName = "session") Session session
    ) throws IOException;

    @WebMethod
    void dataXmlMapperLoad(
            @WebParam(name = "session", partName = "session") Session session
    ) throws IOException;
}