package ru.ovechkin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAuthEndpoint {

    @Nullable
    @WebMethod
    String getUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void login(
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    );

    @WebMethod
    void logout(
            @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void registry(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    @WebMethod
    User showCurrentUserByUserId(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable String userId
    );

    @WebMethod
    void updateProfileInfo(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable String newLogin,
            @Nullable String newFirstName,
            @Nullable String newMiddleName,
            @Nullable String newLastName,
            @Nullable String newEmail
    );

    @WebMethod
    void updatePassword(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable String currentPassword,
            @Nullable String newPassword
    );

}