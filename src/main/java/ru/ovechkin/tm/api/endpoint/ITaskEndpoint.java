package ru.ovechkin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void add(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "task", partName = "task") Task task);

    @WebMethod
    void createTaskWithName(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    void createTaskWithNameAndDescription(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    @Nullable List<Task> findUserTasks(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    void removeAllTasks(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    Task findTaskById(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    @Nullable Task findTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @WebMethod
    @Nullable Task findTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    @Nullable Task updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    @Nullable Task updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    @Nullable Task removeTaskById(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    @Nullable Task removeTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    );

    @WebMethod
    @Nullable Task removeTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

}