package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    {
        entities.add(new User("user", HashUtil.salt("user"), "use@mail.ck"));
        entities.add(new User("admin", HashUtil.salt("admin"), Role.ADMIN));
        entities.add(new User("admin1", HashUtil.salt("admin1"), Role.ADMIN));
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) {
        entities.add(user);
        return user;
    }

    @Override
    public @NotNull User findById(@NotNull final String id) {
        for (@NotNull final User user : entities) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public @NotNull User findByLogin(@NotNull final String login) {
        for (@NotNull final User user : entities) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @NotNull
    @Override
    public User removeUser(@NotNull final User user) {
        entities.remove(user);
        return user;
    }

    @Override
    public @NotNull User removeById(@NotNull final String id) {
        @Nullable final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public @NotNull User removeByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

}