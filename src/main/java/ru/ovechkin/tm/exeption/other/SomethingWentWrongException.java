package ru.ovechkin.tm.exeption.other;

public class SomethingWentWrongException extends RuntimeException{

    public SomethingWentWrongException() {
        super("Error! Something went wrong...");
    }

}