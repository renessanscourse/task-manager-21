package ru.ovechkin.tm.exeption.user;

public class UsersEmptyException extends RuntimeException {

    public UsersEmptyException() {
        super("Error! User's are not created...");
    }

}