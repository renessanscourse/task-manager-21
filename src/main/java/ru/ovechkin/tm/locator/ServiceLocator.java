package ru.ovechkin.tm.locator;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.repository.ISessionRepository;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.api.service.*;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.SessionRepository;
import ru.ovechkin.tm.repository.TaskRepository;
import ru.ovechkin.tm.repository.UserRepository;
import ru.ovechkin.tm.service.*;

@Getter
public class ServiceLocator implements IServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    final IServiceLocator serviceLocator = this;

    @NotNull
    private final ISessionService sessionService = new SessionService(serviceLocator, sessionRepository);

    @NotNull
    private final IStorageService storageService = new StorageService(serviceLocator);

}