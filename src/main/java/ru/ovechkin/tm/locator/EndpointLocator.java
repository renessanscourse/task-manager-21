package ru.ovechkin.tm.locator;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.api.endpoint.*;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.endpoint.*;

@Getter
public class EndpointLocator implements IEndpointLocator {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final ISessionEndpoint sessionEndpoint;

    @NotNull
    private final IUserEndpoint userEndpoint;

    @NotNull
    private final IAuthEndpoint authEndpoint;

    @NotNull
    private final ITaskEndpoint taskEndpoint;

    @NotNull
    private final IProjectEndpoint projectEndpoint;

    @NotNull
    private final IStorageEndpoint storageEndpoint;

    public EndpointLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.sessionEndpoint = new SessionEndpoint(serviceLocator);
        this.userEndpoint = new UserEndpoint(serviceLocator);
        this.authEndpoint = new AuthEndpoint(serviceLocator);
        this.taskEndpoint = new TaskEndpoint(serviceLocator);
        this.projectEndpoint = new ProjectEndpoint(serviceLocator);
        this.storageEndpoint = new StorageEndpoint(serviceLocator);
    }

}