package ru.ovechkin.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Session extends AbstractEntity implements Cloneable {

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @NotNull
    private Long timestamp;

    @NotNull
    private String userId;

    @NotNull
    private String signature;

    public Session() {
    }

    @Override
    public String toString() {
        return "Session{" +
                "timestamp=" + timestamp +
                ", userId='" + userId + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }

}