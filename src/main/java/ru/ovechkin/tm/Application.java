package ru.ovechkin.tm;

import ru.ovechkin.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(final String[] args) throws Exception {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}